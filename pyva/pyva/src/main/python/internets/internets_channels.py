from pseudoclient.collection import *
from pseudoclient.sys_channels import *

class InternetsChannel(CollectionEntity):
	youtube_info = False

	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name

class InternetsChannelManager(ChannelManager):
	def __init__(self, module):
		ChannelManager.__init__(self, module, InternetsChannel)

/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import net.rizon.acid.plugins.vizon.db.VizonDrawing;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
@RunWith(MockitoJUnitRunner.class)
public class VizonDrawingTest
{
	@Mock
	private ResultSet resultSet;

	private VizonDrawing drawing;

	public VizonDrawingTest()
	{
	}

	@Before
	public void setUp()
	{
		try
		{
			when(resultSet.getInt("id")).thenReturn(1);
			when(resultSet.getInt("first")).thenReturn(1);
			when(resultSet.getInt("second")).thenReturn(2);
			when(resultSet.getInt("third")).thenReturn(3);
			when(resultSet.getInt("fourth")).thenReturn(4);
			when(resultSet.getInt("fifth")).thenReturn(5);
			when(resultSet.getInt("sixth")).thenReturn(6);
			when(resultSet.getTimestamp("drawing_date")).thenReturn(Timestamp.valueOf("2016-12-12 00:00:00"));
		}
		catch (SQLException ex)
		{
			fail("SQLException");
		}

		drawing = VizonDrawing.fromResultSet(resultSet);
	}

	/**
	 * Tests if the calculator returns the correct amount of correct values.
	 */
	@Test
	public void zeroCorrectTest()
	{
		Bet bet = new Bet(11, 12, 13, 14, 15, 16);

		int actual = drawing.checkCorrect(bet);
		int expected = 0;

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Tests if the calculator returns the correct amount of correct values.
	 */
	@Test
	public void oneCorrectTest()
	{
		Bet bet = new Bet(1, 12, 13, 14, 15, 16);

		int actual = drawing.checkCorrect(bet);
		int expected = 1;

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Tests if the calculator returns the correct amount of correct values.
	 */
	@Test
	public void twoCorrectTest()
	{
		Bet bet = new Bet(1, 2, 13, 14, 15, 16);

		int actual = drawing.checkCorrect(bet);
		int expected = 2;

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Tests if the calculator returns the correct amount of correct values.
	 */
	@Test
	public void threeCorrectTest()
	{
		Bet bet = new Bet(1, 2, 3, 14, 15, 16);

		int actual = drawing.checkCorrect(bet);
		int expected = 3;

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Tests if the calculator returns the correct amount of correct values.
	 */
	@Test
	public void fourCorrectTest()
	{
		Bet bet = new Bet(1, 2, 3, 4, 15, 16);

		int actual = drawing.checkCorrect(bet);
		int expected = 4;

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Tests if the calculator returns the correct amount of correct values.
	 */
	@Test
	public void fiveCorrectTest()
	{
		Bet bet = new Bet(1, 2, 3, 4, 5, 16);

		int actual = drawing.checkCorrect(bet);
		int expected = 5;

		Assert.assertEquals(expected, actual);
	}

	/**
	 * Tests if the calculator returns the correct amount of correct values.
	 */
	@Test
	public void sixCorrectTest()
	{
		Bet bet = new Bet(1, 2, 3, 4, 5, 6);

		int actual = drawing.checkCorrect(bet);
		int expected = 6;

		Assert.assertEquals(expected, actual);
	}
}

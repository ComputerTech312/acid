/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VizonRequest
{
	private static final Logger logger = LoggerFactory.getLogger(VizonRequest.class);

	private final int id;
	private final int userId;
	private final String nick;
	private String vhost;
	private int status;
	private String reason;
	private String oper;
	private LocalDateTime date;

	public static VizonRequest fromResultSet(ResultSet rs)
	{
		try
		{
			int id = rs.getInt("id");
			int userId = rs.getInt("user_id");
			String vhost = rs.getString("vhost");
			String nick = rs.getString("nick");
			int status = rs.getInt("status");
			String reason = rs.getString("reason");
			String oper = rs.getString("oper");
			LocalDateTime date = rs.getTimestamp("date").toLocalDateTime();

			return new VizonRequest(id, userId, nick, vhost, status, reason, oper, date);
		}
		catch (SQLException ex)
		{
			logger.warn("Unable to construct VizonRequest from ResultSet", ex);
			return null;
		}
	}

	public VizonRequest(int id, int userId, String nick, String vhost, int status, String reason, String oper, LocalDateTime date)
	{
		this.id = id;
		this.userId = userId;
		this.nick = nick;
		this.vhost = vhost;
		this.status = status;
		this.reason = reason;
		this.oper = oper;
		this.date = date;
	}

	public int getId()
	{
		return id;
	}

	public int getUserId()
	{
		return userId;
	}

	public String getNick()
	{
		return nick;
	}

	public String getVhost()
	{
		return vhost;
	}

	public int getStatus()
	{
		return status;
	}

	public String getReason()
	{
		return reason;
	}

	public String getOper()
	{
		return oper;
	}

	public LocalDateTime getDate()
	{
		return date;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

	public void setOper(String oper)
	{
		this.oper = oper;
	}

	public void setDate(LocalDateTime date)
	{
		this.date = date;
	}

	public void setVhost(String vhost)
	{
		this.vhost = vhost;
	}

}

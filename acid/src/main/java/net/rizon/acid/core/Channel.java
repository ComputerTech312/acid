package net.rizon.acid.core;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Channel
{
	private static final Logger log = LoggerFactory.getLogger(Channel.class);

	private int ts;
	private String channel;
	// User list: nick => channel user status modes. TreeMap auto-sorts keys
	private TreeMap<String, Membership> list = new TreeMap<String, Membership>();
	private String modes = "";
	private String key;
	private int limit;

	/*
	 * support for ban-like modes (as the TS6 spec calls them) that include
	 * a mask with them. (+b/+e/+I)
	 */
	private final Set<String> modesBans = new HashSet<>();
	private final Set<String> modesExempt = new HashSet<>();
	private final Set<String> modesInvEx = new HashSet<>();

	public Channel(String channel, int ts)
	{
		this.ts = ts;
		this.channel = channel;

		channelMap.put(channel.toLowerCase(), this);
	}

	public void destroy()
	{
		channelMap.remove(this.channel.toLowerCase());
	}

	public Set<String> getUsers()
	{
		return list.keySet();
	}

	/**
	 * Return a collection of {@link Membership}s for this channel
	 * @return Collection
	 */
	public Collection<Membership> getMembers()
	{
		return list.values();
	}

	public void addUser(User user, String modes)
	{
		list.put(user.getNick().toLowerCase(), new Membership(user, this, modes));
	}

	public void removeUser(User user)
	{
		list.remove(user.getNick().toLowerCase());
		if (list.isEmpty() && !hasMode('z'))
			this.destroy();
	}

	public Membership findUser(User user)
	{
		return list.get(user.getNick().toLowerCase());
	}

	public String getName()
	{
		return channel;
	}

	public int getTS()
	{
		return ts;
	}

	public void reset(int ts)
	{
		log.debug("Lowering TS of " + channel + " from " + this.ts + " to " + ts);

		this.ts = ts;
		for (Membership cm : list.values())
			cm.clear();
		this.modes = "";
		this.key = null;
		this.limit = 0;
	}

	public int size()
	{
		return list.size();
	}

	public void setMode(char mode)
	{
		if (modes.indexOf(mode) == -1)
			modes += mode;
	}

	public void unsetMode(char mode)
	{
		modes = modes.replace("" + mode, "");
	}

	public boolean hasMode(char mode)
	{
		return modes.indexOf(mode) != -1;
	}

	/* interface for message handlers to change ban-like modes */
	public void changeMaskMode(char mode, boolean adding, String mask)
	{
		Set<String> set;

		if (mode == 'b')
			set = modesBans;
		else if (mode == 'e')
			set = modesExempt;
		else if (mode == 'I')
			set = modesInvEx;
		else
			return;

		if (adding)
			set.add(mask);
		else
			set.remove(mask);
	}

	public Set<String> getBans()
	{
		return modesBans;
	}

	public Set<String> getExempts()
	{
		return modesExempt;
	}

	public Set<String> getInvEx()
	{
		return modesInvEx;
	}

	public String getKey()
	{
		return key;
	}

	public int getLimit()
	{
		return limit;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public void setLimit(int limit)
	{
		this.limit = limit;
	}

	public String getModes(boolean full)
	{
		if (!full)
			return modes;
		else
		{
			String m = modes;
			int lim = m.indexOf('l');
			int key = m.indexOf('k');
			if (lim != -1 && key != -1)
				if (lim < key)
					m += " " + limit + " " + key;
				else
					m += " " + key + " " + limit;
			else if (lim != -1)
				m += " " + limit;
			else if (key != -1)
				m += " " + key;
			return m;
		}
	}

	public void onNick(String oldnick, String newnick)
	{
		Membership user = list.get(oldnick.toLowerCase());
		if (user != null)
		{
			list.remove(oldnick.toLowerCase());
			list.put(newnick.toLowerCase(), user);
		}
		else
			log.warn("Channel::onNick unknown nick change for " + oldnick + " -> " + newnick);
	}


	public void listUsers(User target)
	{
		listUsers(target, null, null);
	}

	public void listUsers(User target, AcidUser to, Channel c)
	{
		ArrayList<String> owners = new ArrayList<String>();
		ArrayList<String> admins = new ArrayList<String>();
		ArrayList<String> ops = new ArrayList<String>();
		ArrayList<String> hops = new ArrayList<String>();
		ArrayList<String> voices = new ArrayList<String>();
		ArrayList<String> regs = new ArrayList<String>();
		Membership modes;
		User user;
		for (String s : list.keySet())
		{
			modes = list.get(s);
			user = User.findUser(s);
			if (modes != null && user != null)
			{
				if (modes.hasOwner())
					owners.add(modes.getModes() + user.getNString());
				else if (modes.hasAdmin())
					admins.add(modes.getModes() + user.getNString());
				else if (modes.hasOp())
					ops.add(modes.getModes() + user.getNString());
				else if (modes.hasHalfop())
					hops.add(modes.getModes() + user.getNString());
				else if (modes.hasVoice())
					voices.add(modes.getModes() + user.getNString());
				else
					regs.add(modes.getModes() + user.getNString());
			}
		}
		for (int i = 0; i < owners.size(); i++)
			Acidictive.reply(target, to, c, owners.get(i));
		for (int i = 0; i < admins.size(); i++)
			Acidictive.reply(target, to, c, admins.get(i));
		for (int i = 0; i < ops.size(); i++)
			Acidictive.reply(target, to, c, ops.get(i));
		for (int i = 0; i < hops.size(); i++)
			Acidictive.reply(target, to, c, hops.get(i));
		for (int i = 0; i < voices.size(); i++)
			Acidictive.reply(target, to, c, voices.get(i));
		for (int i = 0; i < regs.size(); i++)
			Acidictive.reply(target, to, c, regs.get(i));
		Acidictive.reply(target, to, c, "Listed " + (owners.size() + admins.size() + ops.size() + hops.size() + voices.size() + regs.size()) + " users that are currently in " + channel);
	}

	/*
	 * LinkedHashMap has a slight advantage when it comes to lookup and value based iteration
	 * which makes it more suitable for things that operate exclusively on all Channel objects
	 * instead of looking up specific channels by key. (there's a few in Geo & friends).
	 */
	private static LinkedHashMap<String, Channel> channelMap = new LinkedHashMap<String, Channel>();

	public static Collection<Channel> getChannelList()
	{
		return channelMap.values();
	}

	public static Set<String> getChannels()
	{
		return channelMap.keySet();
	}

	public static Channel findChannel(final String name)
	{
		return channelMap.get(name.toLowerCase());
	}
}

package net.rizon.acid.plugins;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public final class ClassLoader extends URLClassLoader
{
	private static final Logger logger = LoggerFactory.getLogger(ClassLoader.class);

	private static final File JAR = new File(ClassLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath());

	private final String base;

	public ClassLoader(String base) throws MalformedURLException
	{
		this(JAR, base);
	}

	public ClassLoader(File jar, String base) throws MalformedURLException
	{
		// Set parent classloader to null so we can tell whether or not
		// the class comes from this urlclassloader, or the systems
		super(new URL[0], null);

		addFile(jar);

		this.base = base;
	}

	public void addFile(File f) throws MalformedURLException
	{
		this.addURL(f.toURI().toURL());
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException
	{
		return loadClassRecurse(name, true);
	}

	private Class<?> loadClassRecurse(String name, boolean recurse) throws ClassNotFoundException
	{
		// Always reload from the urlloader anything under base
		if (name.startsWith(base) == true)
		{
			Class<?> c = super.loadClass(name);
			logger.debug("Loaded class {} from classloader of base {}", c, base);
			return c;
		}

		// Second, see if system classloader can find it, and prefer that
		// These can never be reloaded
		try
		{
			Class<?> c = getSystemClassLoader().loadClass(name);
			logger.debug("Loaded class {} from system class loader", c);
			return c;
		}
		catch (ClassNotFoundException ex)
		{
		}

		// If system loader can't find it it is either another plugin's class,
		// a dependency of another plugin, or a dependency of this plugin
		if (recurse)
		{
			// Search the other plugins. If it is a dependency of both the
			// other plugin and this plugin, we will prefer the other plugin's.

			for (Plugin p : PluginManager.getPlugins())
			{
				try
				{
					Class<?> c = p.loader.loadClassRecurse(name, false);
					logger.debug("Loaded class {} from plugin {}", c, p);
					return c;
				}
				catch (ClassNotFoundException ex)
				{
				}
			}
		}

		// This is a dependency of this plugin
		Class<?> c = super.loadClass(name);
		logger.debug("Loaded class {} from url loader", c);
		return c;
	}
}

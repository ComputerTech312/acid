package net.rizon.acid.events;

import net.rizon.acid.core.User;

/**
 * Event used when a user is introduced to the network. This can be either from
 * a connect or after a burst.
 *
 * @author orillion <orillion@rizon.net>
 */
public class EventUserIntroduce
{
	private final User user;

	public EventUserIntroduce(User user)
	{
		this.user = user;
	}

	public User getUser()
	{
		return user;
	}
}

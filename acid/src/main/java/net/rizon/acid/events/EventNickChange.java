package net.rizon.acid.events;

import net.rizon.acid.core.User;

public class EventNickChange
{
	private User u;
	private String oldNick;

	public User getU()
	{
		return u;
	}

	public void setU(User u)
	{
		this.u = u;
	}

	public String getOldNick()
	{
		return oldNick;
	}

	public void setOldNick(String oldNick)
	{
		this.oldNick = oldNick;
	}
}

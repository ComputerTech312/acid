/*
 * Copyright (C) 2017 Kristina Brooks. All rights reserved.
 *
 * Support for TS6 BMASK
 * Message format: chanTS, chan, type, space separated masks
 * Plexus only supports +b, +e and +I.
 */

package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BMask extends Message
{
	private static final Logger log = LoggerFactory.getLogger(BMask.class);

	public BMask()
	{
		super("BMASK");
	}

	@Override
	public void onServer(Server server, String[] params)
	{
		Channel chan = Channel.findChannel(params[1]);
		if (chan == null || shouldDropTsMessage(chan.getTS(), params[0]))
			return;

		char mode = params[2].charAt(0);

		String[] splitMasks = params[3].split(" ");
		for (String mask : splitMasks)
		{
			chan.changeMaskMode(mode, true, mask);
		}
	}
}
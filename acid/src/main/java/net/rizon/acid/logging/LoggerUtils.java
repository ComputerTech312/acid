package net.rizon.acid.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerUtils
{
	private static final Logger log = LoggerFactory.getLogger(LoggerUtils.class);

	public static void initUncaughtExceptionHandler()
	{
		Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
			log.error("uncaught exception in " + t, e);
		});
	}

	public static void initThread(final Logger logger, Thread t)
	{
		t.setUncaughtExceptionHandler((t1, e) -> {
			logger.error("uncaught exception in " + t1, e);
		});
	}
}

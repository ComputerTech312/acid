package net.rizon.acid.arguments;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests invalid arguments to the Expiry Argument.
 *
 * @author orillion <orillion@rizon.net>
 */
public class ExpiryArgumentFailTest
{


	/**
	 * Tests whether this returns null when the argument is not prefixed by a +
	 * character.
	 */
	@Test
	public void NoPlusTest()
	{
		String arg = "30d";

		ExpiryArgument argument = ExpiryArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether this returns null when the argument contains more than 1
	 * time definition.
	 */
	@Test
	public void TwoArgumentsTest()
	{
		String arg = "+30d5h";

		ExpiryArgument argument = ExpiryArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether this returns null when you enter null.
	 */
	@Test
	public void NullTest()
	{
		String arg = null;

		ExpiryArgument argument = ExpiryArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether this returns null when no valid option is inserted.
	 */
	@Test
	public void NoValidOption()
	{
		String arg = "+30a";

		ExpiryArgument argument = ExpiryArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether this returns null when a negative time span is entered.
	 */
	@Test
	public void NegativeDurationTest()
	{
		String arg = "+-30d";

		ExpiryArgument argument = ExpiryArgument.parse(arg);

		Assert.assertNull(argument);
	}

	/**
	 * Tests whether this returns null when a zero time span is entered.
	 */
	@Test
	public void ZeroDurationTest()
	{
		String arg = "+0d";

		ExpiryArgument argument = ExpiryArgument.parse(arg);

		Assert.assertNull(argument);
	}
}
